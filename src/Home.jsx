import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import { useOktaAuth } from '@okta/okta-react';

const Home = () => {
  const { authState, oktaAuth } = useOktaAuth();
  const history = useHistory();
  const [response , setResponse] = useState("");

  React.useEffect(() => {
    var code = getQueryVariable("code")

    // perform callback to complete auth flow 
    if(code)
    {
      fetch('http://myid.cheetahces.com/oauth2/token', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        withCredentials : true , 
        credentials: 'include',
        body: JSON.stringify({
          clientId : 'def769', 
          clientSecret : 'abc123' , 
          code : code , 
          scopes : 'email'
        })
      })
      .then(response => response.json())
      .then((data) => {
  
        setResponse(data.access_token)
      })
      .catch((err)=>{
        setResponse("Session not found")
      })
    }
  }, [])

  const handleClick = () => {

    const redirectUri = 'http://cust100.cheetahedp.com'
    const responseType = 'code'
    const state = 'abc123'  // should be randomly generated 

    window.location.replace(
      `http://myid.cheetahces.com/oauth2/authorize?redirectUri=${redirectUri}&state=${state}&responseType=${responseType}`
    )

    /*
    fetch('http://myid.cheetahces.com/user/token/testSession', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      credentials: 'include',
      body: JSON.stringify({
      })
    })
    .then(response => response.json())
    .then((data) => {

      setResponse(data.resp)
    })
    .catch((err)=>{
      setResponse("Cors Error")
    })*/
  }

  function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == variable) {
            return decodeURIComponent(pair[1]);
        }
    }
}

  return (
    <div>
      <h2>Welcome to EDP Cust100</h2>

      <button  onClick={() => {handleClick()}}>Initiate Auth Code flow to Request Token</button>
    
      <p>
        {response}
      </p>
    </div>
  );
};
export default Home;
